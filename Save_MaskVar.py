"""
Save out a netcdf file containing a mask variable. 

This has NaNs over land and 1's over ocean.


    $ python DiffComparison_Global.py var season interp
with command line arguments:
    var -- LHFLX
    season -- ANN 
    interp -- bilin

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
#
from SignificanceBenchmark_GlobalMap import *
###########################################

def main():
    #
    vo = 'LHFLX'
    so = 'ANN'
    io = 'bilin'
    var_names = parse_var(vo)
    season = check_season(so)
    interp = check_interp(io)
    #
    # Load data.
    print('\nReading data...\n')
    mask_var = load_data(var_names, season, interp)
    #
    # Save out file.
    print('\Saving file... \n')
    ds_out = xr.Dataset({'ocean_mask':mask_var})
    ds_out.attrs = {'title':'Ocean mask on 1x1 degree regular grid',
                    'creator_name':'Jack Reeves Eyre',
                    'creation_time':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    'source_code':'Save_MaskVar.py',
                    'code_options':'var_names=' + vo + ', season=' + so + ', interp=' + io}
    wr_enc = {'ocean_mask':{'_FillValue':9.969209968386869e+36}}
    ds_out.to_netcdf(path='/home/jackre/Data/maps/masks/ocean_mask_180x360.nc',
                     mode='w',
                     encoding=wr_enc)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'LHFLX':
        var_name_dict['atmo'] = 'LHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_latentHeatFlux'
        var_name_dict['CMIP'] = 'hfls'
    elif v == 'SHFLX':
        var_name_dict['atmo'] = 'SHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_sensibleHeatFlux'
        var_name_dict['CMIP'] = 'hfss'
    elif v == 'TAUX':
        var_name_dict['atmo'] = 'TAUX'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressZonal'
        var_name_dict['CMIP'] = 'tauu'
    elif v == 'TAUY':
        var_name_dict['atmo'] = 'TAUY'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressMeridional'
        var_name_dict['CMIP'] = 'tauv'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_data(v,s,i):
    """Loads data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    # Define file names and data types:
    ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/'
    ctl_o_dir = '/home/jackre/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/'
    UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/'
    UA_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/'
    COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/'
    COARE_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/'
    filenames = [glob.glob(UA_a_dir +
                           '20190521.v1_UA.minus.20190521.v1_ctl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(UA_o_dir +
                           '20191008.ocnSfcFlx_UA.minus.' +
                           '20191007.ocnSfcFlx_ctl_mpaso_' +
                           s + '_*_climo.nc')]
    data_types = ['atmo','ocn']
    #
    # Define plot titles based on above files.
    plot_names = ['atmosphere: UA - control',
                  'ocean: UA - control']
    #
    # Read data.
    data_dict = {}
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        if data_types[i_fn]=='atmo':
            data_dict[plot_names[i_fn]] = -1.0*ds[v[data_types[i_fn]]][0,:,:]
        else:
            data_dict[plot_names[i_fn]] = ds[v[data_types[i_fn]]][0,:,:]
    #
    # Calculate the mask.
    mask_var_name = 'ocean: UA - control'
    thresh = 0.00001
    mask_var = np.where(np.abs(data_dict[mask_var_name].data) < thresh,
                        np.nan, 1.0)
    #
    # Put mask in an xarray data array.
    mask_array = xr.full_like(data_dict[mask_var_name], np.nan, np.float64)
    mask_array.data = mask_var
    mask_array.attrs = {
        'units':'none',
        'standard_name':'ocean_mask',
        'long_name':'Ocean mask: ocean=1, land=NaN',
        'method':'land = where difference between model climatologies is smaller than threshold',
        'threshold':thresh,
        'model_difference':mask_var_name,
        'model_variable':v['ocn']}
    #
    # Return data.
    return(mask_array)


################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
