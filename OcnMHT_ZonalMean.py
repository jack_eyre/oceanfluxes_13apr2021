"""
Plot zonal means of ocean meridional heat transport from different 
model runs. 

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
import calendar
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Load data.
    print('-----\nReading data...\n-----')
    MHT_data,run_names = load_MHT_1D()
    obs_data = load_MHT_obs_1D()
    #
    # Plot results.
    print('-----\nPlotting...\n-----')
    lat_range = (-90.0,90.0)
    plot_zonal_means(MHT_data,run_names,
                     obs_data,
                     lat_range)
    #
    return

################################################################################
# Functions to load and process data.
################################################################################


def load_MHT_1D():
    #
    # Define cases.
    cases = ['20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil',
             '20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil',
             '20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil']
    case_names = ['control',
                  'UA',
                  'COARE']
    #
    # Initialize variable to hold data.
    data_dict = {}
    # Load files.
    for ic,c in enumerate(cases):
        file_name = '~/Data/E3SM/' + c + '/ocn/' + \
                    'meridionalHeatTransport_years0002-0010.nc'
        ds = xr.open_dataset(file_name)
        data_dict[case_names[ic]] = \
            ds['timeMonthly_avg_meridionalHeatTransportLat']
    #
    return data_dict,case_names

def load_MHT_obs_1D():
    #
    # Load file.
    file_name = '~/Data/ocean_obs/TrenberthCaronMHT/' + \
                'mht_TrenberthCaron.NoAtm.nc'
    ds = xr.open_dataset(file_name)
    #
    # Read data into dictionary.
    data_dict = {}
    data_dict['mean'] = ds['GLOBALECMWF_ADJUSTED']
    data_dict['upper'] = ds['GLOBALECMWF_ADJUSTED'] + ds['GLOBALECMWF_ERR']
    data_dict['lower'] = ds['GLOBALECMWF_ADJUSTED'] - ds['GLOBALECMWF_ERR']
    data_dict['latitude'] = ds['LATITUDE']
    #
    output_dict = {'TrenberthCaron_ECMWF':data_dict}
    return output_dict

################################################################################
# Functions to plot data.
################################################################################

def plot_zonal_means(data_dict, data_names, obs_data, lat_range):
    #
    # Define colors.
    custom_cols = ['#268bd2',
                   '#6c71c4',
                   '#dc322f',
                   '#b58900']
    #
    # Set up panel and axes.
    fig,ax = plt.subplots(1,1,
                          figsize=(8,5))
    #
    # Plot the observational data.
    for i_k, k in enumerate(list(obs_data.keys())):
        ax.plot(obs_data[k]['latitude'],
                obs_data[k]['mean'],
                color='#657b83',
                label=k)
        ax.fill_between(obs_data[k]['latitude'],
                        obs_data[k]['lower'],
                        obs_data[k]['upper'],
                        color='#657b83',alpha=0.3)
    #
    # Plot the model data.
    for i_c, c in enumerate(data_names):
        ax.plot(data_dict[c].coords['binBoundaryMerHeatTrans'],
                data_dict[c],
                color=custom_cols[i_c],
                label=c)
    #
    # Plot details.
    ax.set_xlim(lat_range[0],lat_range[1])
    ax.set_ylim(-2.5,2.5)
    ax.set_ylabel('MHT / ' + r'$PW$')
    ax.set_xlabel('latitude / ' + r'$^{\circ}N$')
    ax.xaxis.set_tick_params(which='both',
                             bottom=True,top=True)
    ax.yaxis.set_tick_params(which='both',
                             left=True,right=True)
    ax.plot([lat_range[0],lat_range[1]],[0.0,0.0],
            color='#93a1a1',linestyle='--',linewidth=0.5)
    #
    # Add legend.
    ax.legend(loc='upper left')
    #      
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    #
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Show the plot.
    #plt.show()
    # Save the figure.
    plt.savefig(os.path.expanduser('~') +
                '/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    #
    return

    
################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
