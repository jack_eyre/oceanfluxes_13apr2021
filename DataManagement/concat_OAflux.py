"""
Concatenates all OAflux files (1 per year) into a single file per variable.
"""

###########################################
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import sys
import os
###########################################

# Get git info for this script.
sys.path.append(os.path.expanduser('~/Documents/code/'))
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))

###########################################

def main():
    #
    # Define some details.
    st_yr = 1958
    end_yr = 2018
    data_dir = '/home/jackre/Data/ocean_obs/OAflux/'
    filename_vars = ['sh','lh']
    file_vars = ['shtfl','lhtfl']
    filename_root = '_oaflux_'
    #
    # Loop over variables.
    for iv, vv in enumerate(filename_vars):
        #
        # Read first file.
        f1 = xr.open_dataset(data_dir + vv + filename_root +
                             "{:04d}".format(st_yr) + '.nc')
        filelist = vv + filename_root + "{:04d}".format(st_yr) + '.nc'
        #
        # Create new array to hold entire time series.
        n_yrs = 1 + end_yr - st_yr
        n_lat = len(f1.lat)
        n_lon = len(f1.lon)
        ts = xr.DataArray(np.nan*np.zeros((12*n_yrs, n_lat, n_lon)),
                          coords={'time':pd.date_range(
                              start="{:04d}".format(st_yr) + '-01-01',
                              periods=12*n_yrs,
                              freq='MS'),
                                  'lat':f1.lat,
                                  'lon':f1.lon},
                          dims=['time','lat','lon'])
        #
        # Get years and months for easier indexing.
        years = ts.time.dt.year
        months = ts.time.dt.month
        #
        # Add data from first file.
        ts[(years == st_yr),:,:] = f1[file_vars[iv]].data
        #
        # Loop over other years and add data.
        for y in range(st_yr+1, end_yr+1):
            fy = xr.open_dataset(data_dir + vv + filename_root +
                                 "{:04d}".format(y) + '.nc')
            ts[(years == y),:,:] = fy[file_vars[iv]].data
            filelist = filelist + ', ' + \
                       vv + filename_root + "{:04d}".format(y) + '.nc'
        #
        # Create data set to save as file.
        ds_out = xr.Dataset({file_vars[iv]:(['time','lat','lon'],ts)},
                            coords={'time':(['time'],ts.coords['time']),
                                    'lat':(['lat'],ts.coords['lat']),
                                    'lon':(['lon'],ts.coords['lon'])})
        #
        # Add attributes to data variables and file.
        ds_out[file_vars[iv]].attrs = f1[file_vars[iv]].attrs
        ds_out.coords['lat'].attrs = f1.coords['lat'].attrs
        ds_out.coords['lon'].attrs = f1.coords['lon'].attrs
        ds_out.attrs = f1.attrs
        ds_out.attrs['pp_description'] = 'Concatenated OAflux files.'
        ds_out.attrs['pp_filelist'] = filelist
        ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
        ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/oceanfluxes/src/master/'
        ds_out.attrs['pp_last_commit'] = last_hash                               
        ds_out.attrs['pp_script'] = rel_path       
        ds_out.attrs['pp_script_status'] = info  
        ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV'] 
        ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #
        # Set up encoding.                       
        t_units = 'days since 1900-01-01 00:00:00'
        t_cal = 'standard'                       
        fill_val = 9.96921e+36                   
        wr_enc = {file_vars[iv]:{'_FillValue':fill_val},
                  'time':{'units':t_units,'calendar':t_cal,
                          '_FillValue':fill_val},
                  'lat':{'_FillValue':fill_val},
                  'lon':{'_FillValue':fill_val}}
        print(ds_out)
        #
        # Save out the file.
        new_file_name = data_dir + file_vars[iv] + filename_root + \
                        "{:04d}".format(st_yr) + '-' + \
                        "{:04d}".format(end_yr) + '.nc'
        ds_out.to_netcdf(path=new_file_name, mode='w',
                         encoding=wr_enc,unlimited_dims=['time'])
        #
    return


################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
