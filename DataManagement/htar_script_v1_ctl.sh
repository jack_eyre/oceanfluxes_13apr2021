#!/bin/bash -l
#SBATCH -q xfer
#SBATCH -t 12:00:00
#SBATCH -J ctl_flux_archive

# -------------------------------
# Submit job with
#     module load esslurm
#     sbatch <job_script>
# -------------------------------
#
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/hist/
export HPSSDIR=/home/j/jeyre/ocn_surface_flux/${CASENAME}/
#
htar -cvf ${HPSSDIR}${CASENAME}.cam.h0.0001-0006.tar ${DATADIR}*h0*
for y in `seq 1 5`; 
do
    htar -cvf ${HPSSDIR}${CASENAME}.cam.h1.000${y}.tar ${DATADIR}*h1.000${y}*
done
htar -cvf ${HPSSDIR}${CASENAME}.cam.h1.0006.tar ${DATADIR}*h1.000[6-7]*
htar -cvf ${HPSSDIR}${CASENAME}.cam.h2.0001-0006.tar ${DATADIR}*h2*
#
# -------------------------------

