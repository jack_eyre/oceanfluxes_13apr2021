"""
JRA data location on Blues:
/home/ccsm-data/inputdata/ocn/jra55/v1.3_noleap

File names:
JRA.v1.3.u_10.TL319.1958.171019.nc
etc.
About 2.3GB each.

NCDUMP output:
netcdf JRA.v1.3.u_10.TL319.1958.171019 {
dimensions:
        time = UNLIMITED ; // (2920 currently)
        longitude = 640 ;
        latitude = 320 ;
variables:
        double time(time) ;
                time:long_name = "time" ;
                time:units = "hours since 1958-01-01 00:00:00" ;
                time:cartesian_axis = "T" ;
                time:calendar = "noleap" ;
        double longitude(longitude) ;
                longitude:long_name = "longitude" ;
                longitude:units = "degrees_east" ;
                longitude:cartesian_axis = "X" ;
                longitude:point_spacing = "even" ;
        double latitude(latitude) ;
                latitude:long_name = "latitude" ;
                latitude:units = "degrees_north" ;
                latitude:cartesian_axis = "Y" ;
                latitude:point_spacing = "uneven" ;
        float u_10(time, latitude, longitude) ;
                u_10:note = "10m reference height data" ;
                u_10:units = "m/s" ;
                u_10:long_name = "Eastward Near-Surface Wind" ;
                u_10:standard_name = "eastward_wind" ;
                u_10:missing_value = -9.99e+33f ;
                u_10:_FillValue = -9.99e+33f ;

// global attributes:
                :title = "u_10 -- JRA-55 based atmosphere data on TL319 grid" ;
                :Source = "Tsujino et al., 2017. JRA-55 Based Surface Dataset for Driving Ocean-Sea-Ice Models. Ocean Modell.\n",
                        "Kobayashi et al., 2015. The JRA-55 Reanalysis. J. Met. Soc. Jap., 93, 5-48 " ;
                :Conventions = "CF1.0" ;
                :history = "Original file u_10.1958.18Oct2017.nc reformatted by whokim Thu Oct 19 23:26:11 MDT 2017" ;
                :notes = "JRA-55 TL319 data (v1.3, 2017 Oct 18) reformatted for use in CESM, removed all Feb 29\\\'s, calendar now noleap" ;
}
"""

###########################################
#
# To get these packages on blues, run:
#     source /lcrc/soft/climate/e3sm-unified/load_latest_e3sm_unified.sh
#
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import sys
import os
###########################################


# Configurable values.
st_yr = 1959
end_yr = 1967
data_out_dir = '/home/ac.jeyre/derived_data/OceanFluxes/'
filename_out = data_out_dir + 'JRA.v1.3.Umag_10.TL319.' + \
               str(st_yr) + '-' + str(end_yr) + '.nc'

###########################################
# Main function.

def main():
    # Details of input data.
    data_in_dir = '/home/ccsm-data/inputdata/ocn/jra55/v1.3_noleap/'
    filename_head_u = 'JRA.v1.3.u_10.TL319.'
    filename_head_v = 'JRA.v1.3.v_10.TL319.'
    filename_tail = '.171019.nc'
    # Read first files for array sizes.
    print('---------- Loading first file ----------')
    u_ds = xr.open_dataset(data_in_dir +
                           filename_head_u + str(st_yr) + filename_tail)
    # Create array to hold monthly averages.
    print('---------- Creating monthly arrays ----------')
    U_mag_monthly_data = np.zeros([12*(1+end_yr-st_yr),
                                   len(u_ds.coords['latitude']),
                                   len(u_ds.coords['longitude'])])
    U_mag_monthly = xr.DataArray(U_mag_monthly_data,
                                 coords=[np.arange(str(st_yr) + '-01',
                                                   str(end_yr+1) + '-01',
                                                   dtype='datetime64[M]'
                                 ).astype('datetime64[ns]'),
                                         u_ds.coords['latitude'],
                                         u_ds.coords['longitude']],
                                 dims=['time','latitude','longitude'])
    u_monthly = xr.full_like(U_mag_monthly, np.nan)
    v_monthly = xr.full_like(U_mag_monthly, np.nan)
    # Loop over years and get data.
    print('---------- Loading annual files ----------')
    file_list = '| '
    for y in range(st_yr, end_yr+1):
        print(y)
        file_list = file_list + filename_head_u + str(y) + filename_tail + \
                    ' / ' + \
                    filename_head_u + str(y) + filename_tail + \
                    ' | '
        u_ds = xr.open_dataset(data_in_dir +
                               filename_head_u + str(y) + filename_tail)
        v_ds = xr.open_dataset(data_in_dir +
                               filename_head_v + str(y) + filename_tail)
        U_mag = xr.full_like(u_ds['u_10'], np.nan)
        U_mag.data = np.sqrt(u_ds['u_10']**2 + v_ds['v_10']**2)
        U_mag_monthly.loc[dict(time=(U_mag_monthly.time.dt.year == y))] = \
            U_mag.groupby('time.month').mean()
        u_monthly.loc[dict(time=(u_monthly.time.dt.year == y))] = \
            u_ds['u_10'].groupby('time.month').mean()
        v_monthly.loc[dict(time=(v_monthly.time.dt.year == y))] = \
            u_ds['u_10'].groupby('time.month').mean()
        if y == end_yr:
            u_monthly.attrs = u_ds['u_10'].attrs
            v_monthly.attrs = v_ds['v_10'].attrs
    #
    # Construct output dataset.
    print('---------- Constructing output dataset ----------')
    ds_out = xr.Dataset({'Umag_10':U_mag_monthly,
                         'u_10':u_monthly,
                         'v_10':v_monthly})
    # Add attributes.
    ds_out['Umag_10'].attrs['long_name'] = '10-meter wind speed'
    ds_out['Umag_10'].attrs['units'] = 'm/s'
    ds_out['Umag_10'].attrs['standard_name'] = 'wind_speed'
    ds_out['Umag_10'].attrs['height'] = '10 m'
    ds_out['Umag_10'].attrs['method'] = 'wind speed = sqrt(u^2 + v^2)'
    ds_out['Umag_10'].attrs['comment'] = 'Monthly average wind speed calculated as mean of 3-hourly wind speeds.'
    #
    ds_out.attrs = u_ds.attrs
    ds_out.attrs['post-processing_title'] = 'Monthly average 10-m wind speeds calculated from 3-hourly data.'
    ds_out.attrs['pp_time'] =  datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    ds_out.attrs['pp_creator'] = 'Jack Reeves Eyre, CICOES/UW (previously U. Arizona)'
    ds_out.attrs['pp_file_source'] = 'ANL LCRC Blues: ' + data_in_dir
    ds_out.attrs['pp_file_list'] = file_list
    ds_out.attrs['pp_code'] = 'JRA_climatology.py'
    # Set up encoding.
    t_units = 'days since 1900-01-01 00:00:00'
    t_cal = 'standard'                       
    fill_val = 9.96921e+36                   
    wr_enc = {'Umag_10':{'_FillValue':fill_val},
              'u_10':{'_FillValue':fill_val},
              'v_10':{'_FillValue':fill_val},
              'time':{'units':t_units,'calendar':t_cal,
                      '_FillValue':fill_val, 'dtype':'float64'},
              'latitude':{'_FillValue':fill_val},
              'longitude':{'_FillValue':fill_val}}
    # Write the file.
    print('---------- Writing output file ----------')
    ds_out.to_netcdf(path=filename_out, mode='w',
                     encoding=wr_enc,unlimited_dims=['time'])
    #
    return


################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
