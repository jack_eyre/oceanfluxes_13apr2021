"""
Calculate global means from MPAS ocean output.
"""

###########################################
import numpy as np
import xarray as xr
from itertools import product
from cftime import DatetimeNoLeap
import datetime
import calendar
import os
import sys
import subprocess
###########################################


def main():
    #
    # Get case name.
    case_name = sys.argv[1]
    print(case_name)
    print('Setting up...')
    #
    # Get grid area information.
    area_file = '/lcrc/group/acme/jeyre/archive/' + \
                case_name + \
                '/rest/0001-01-06-00000/' + \
                'mpaso.rst.0001-01-06_00000.nc'
    area_ds = xr.open_dataset(area_file)
    area_2D = area_ds['areaCell']
    area_sum = area_2D.sum()
    #
    # Data locations.
    data_dir = '/lcrc/group/acme/jeyre/archive/' + case_name + '/ocn/hist/'
    save_dir = '/lcrc/group/acme/jeyre/archive/' + case_name + '/ocn/'
    #
    # Filenames.
    filename_root = 'mpaso.hist.am.timeSeriesStatsMonthly.'
    filename_tail = '-01.nc'
    new_filename = case_name + '_mpaso_customGlobalStats_meanAbsolute.nc'
    #
    # Specify time period and create date/time array.
    st_yr = 1
    end_yr = 10
    datelist = [DatetimeNoLeap(year, month, 1) for year, month in
                product(range(st_yr, end_yr+1), range(1, 13))]
    dates = xr.DataArray(datelist, coords=[datelist], dims='Time')
    #
    # Specify variables to extract.
    raw_vars = ['timeMonthly_avg_avgGlobalStats_evaporationFluxAvg',
                'timeMonthly_avg_avgGlobalStats_salinityRestoringFluxAvg']
    avg_vars = ['timeMonthly_avg_evaporationFlux',
                'timeMonthly_avg_evaporationFlux',
                'timeMonthly_avg_latentHeatFlux',
                'timeMonthly_avg_sensibleHeatFlux',
                'timeMonthly_avg_shortWaveHeatFlux',
                'timeMonthly_avg_longWaveHeatFluxUp',
                'timeMonthly_avg_longWaveHeatFluxDown',
                'timeMonthly_avg_pressureAdjustedSSH',
                'timeMonthly_avg_salinitySurfaceRestoringTendency']
    s3D_vars = ['timeMonthly_avg_activeTracers_temperature',
                'timeMonthly_avg_activeTracers_salinity',
                'timeMonthly_avg_velocityMeridional',
                'timeMonthly_avg_velocityZonal']
    #
    # Create dictionaries and data arrays to hold results.
    raw_data = {}
    for v in raw_vars:
        raw_data[v] = array_init(dates, v)
    avg_data = {}
    for v in avg_vars:
        avg_data[v] = array_init(dates, v)
    s3D_data = {}
    for v in s3D_vars:
        s3D_data[v] = array_init(dates, v)
    #
    # Loop over time (one file per month).
    print('Reading data...')
    load_attrs = True
    for yy in range(st_yr,end_yr+1):
        for mm in range(1,13):
            #
            # Get time index of this month.
            iym = (dates.dt.year == yy) & (dates.dt.month == mm)
            #
            # Load file.
            filename =  data_dir + filename_root + \
                        "{:04d}".format(yy) + '-' + \
                        "{:02d}".format(mm) + \
                        filename_tail
            print(filename)
            ds = xr.open_dataset(filename)                       
            #                                                
            # Read already-averaged data.
            for v in raw_vars:
                raw_data[v][iym] = ds[v].data
            #
            # Read 2D data and average.
            for v in avg_vars:
                field = ds[v].isel(Time=0)
                field = abs(field)
                avg_data[v][iym] = ((field*area_2D).sum()/area_sum).data
            #
            # Read 3D data and average.
            for v in s3D_vars:
                field = ds[v].isel(Time=0, nVertLevels=0)
                field = abs(field)
                s3D_data[v][iym] = ((field*area_2D).sum()/area_sum).data
            #
            # Load attributes on first loop.
            if load_attrs:
                for v in raw_vars:                  
                    raw_data[v].attrs = ds[v].attrs 
                for v in avg_vars:                  
                    avg_data[v].attrs = ds[v].attrs                     
                    avg_data[v].attrs['statistical_processing'] = \
                        '(1) gridpoint absolute value; (2) global mean.'
                for v in s3D_vars:                  
                    s3D_data[v].attrs = ds[v].attrs                    
                    s3D_data[v].attrs['statistical_processing'] = \
                        '(1) gridpoint absolute value; (2) global mean.'
                load_attrs = False
        #       
    #
    print('Creating dataset...')
    # Set up encoding and postprocess arrays into dataset.
    ds_out = xr.Dataset({raw_vars[0]: (['Time'], raw_data[raw_vars[0]])},
                        coords={'Time':(['Time'],dates)})
    ds_out[raw_vars[0]].attrs = raw_data[raw_vars[0]].attrs
    t_units = 'days since 0000-01-01 00:00:00'
    t_cal = 'noleap'
    fill_val = 9.96921e+36
    wr_enc = {raw_vars[0]:{'_FillValue':fill_val},
              'Time':{'units':t_units,'calendar':t_cal,
                      '_FillValue':fill_val}}
    for v in raw_vars[1:]:
        ds_out[v] = raw_data[v]
        ds_out[v].attrs = raw_data[v].attrs
        wr_enc[v] = {'_FillValue':fill_val}
    for v in avg_vars:
        ds_out[v] = avg_data[v]
        ds_out[v].attrs = avg_data[v].attrs
        wr_enc[v] = {'_FillValue':fill_val}
    for v in s3D_vars:
        ds_out[v] = s3D_data[v]
        ds_out[v].attrs = s3D_data[v].attrs
        wr_enc[v] = {'_FillValue':fill_val}
    #
    # Add global metadata.
    ds_out.attrs['case_name'] = case_name
    ds_out.attrs['pp_comment'] = 'Global averages calculated and concatenated by Jack Reeves Eyre (University of Arizona)'
    ds_out.attrs['pp_script'] = 'global_means_mpas.py'
    ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/oceanfluxes/src/master/'
    ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #
    # Save data.
    print('Saving file...')
    ds_out.to_netcdf(path=save_dir+new_filename, mode='w',
                     encoding=wr_enc,unlimited_dims=['Time'])
    #
    return

##########
# Function to create empty arrays.
def array_init(dates, varname):
    #
    da = xr.DataArray(np.zeros(len(dates)),
                      coords=[dates], dims=['Time'],
                      name=varname)
    da.data[:] = np.nan
    return da

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
