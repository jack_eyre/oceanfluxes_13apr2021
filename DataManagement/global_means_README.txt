#### Software on Anvil:
    source /lcrc/soft/climate/e3sm-unified/load_latest_e3sm_unified.sh
#### Software on Cori:
    source /global/cfs/cdirs/e3sm/software/anaconda_envs/load_latest_e3sm_unified.sh

#### MPAS variables:
    # Check if these two give the same answer:
    timeMonthly_avg_avgGlobalStats_evaporationFluxAvg
    timeMonthly_avg_evaporationFlux
    # Global means:
    timeMonthly_avg_avgGlobalStats_salinityRestoringFluxAvg
    # Variables to average myself:
    timeMonthly_avg_latentHeatFlux
    timeMonthly_avg_sensibleHeatFlux
    timeMonthly_avg_shortWaveHeatFlux
    timeMonthly_avg_longWaveHeatFluxUp
    timeMonthly_avg_longWaveHeatFluxDown
    timeMonthly_avg_pressureAdjustedSSH
    timeMonthly_avg_activeTracers_temperature
    timeMonthly_avg_activeTracers_salinity
    timeMonthly_avg_velocityMeridional
    timeMonthly_avg_velocityZonal
####
#### Salinity restoring done manually because output is all zeros for
#    UA and COARE runs.
    timeMonthly_avg_salinitySurfaceRestoringTendency
    
#### CAM variables:
    LHFLX
    SHFLX
    TAUX
    TAUY
    FSNS
    FSDS
    FLNS
    FLDS
    QFLX
    PRECC + PRECL
    TREFHT
    TREFHT - TS
    QREFHT
    U10
    

#### Time series etc. from MPAS-analysis

scp jeyre@blues.lcrc.anl.gov:/home/jeyre/mpas_analysis/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/clim/mpas/avg/meridionalHeatTransport_years0002-0010.nc ~/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/

scp jeyre@blues.lcrc.anl.gov:/home/jeyre/mpas_analysis/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/timeseries/moc/mocTimeSeries_0001-0010.nc ~/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/

scp jeyre@blues.lcrc.anl.gov:/home/jeyre/mpas_analysis/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/clim/mpas/avg/meridionalHeatTransport_years0002-0010.nc ~/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/

scp jeyre@blues.lcrc.anl.gov:/home/jeyre/mpas_analysis/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/timeseries/moc/mocTimeSeries_0001-0010.nc ~/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/


--- DOES NOT EXIST!! ----
--- Rerun MPAS-analysis?
scp jeyre@blues.lcrc.anl.gov:/home/jeyre/mpas_analysis/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/clim/mpas/avg/meridionalHeatTransport_years0002-0010.nc ~/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/

scp jeyre@blues.lcrc.anl.gov:/home/jeyre/mpas_analysis/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/timeseries/moc/mocTimeSeries_0001-0010.nc ~/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/

