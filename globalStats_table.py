"""
Print global stats from different model runs.

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
import cftime
import calendar
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Define comparison period
    st_time_atmo = cftime.DatetimeNoLeap(2,1,1)
    end_time_atmo = cftime.DatetimeNoLeap(6,12,31)
    st_time_ocn = cftime.DatetimeNoLeap(2,1,1)
    end_time_ocn = cftime.DatetimeNoLeap(6,12,31)
    st_time_ssh = cftime.DatetimeNoLeap(10,1,1)
    end_time_ssh = cftime.DatetimeNoLeap(10,12,31)
    #
    # Define casenames.
    shortnames_ocn = ['ocn_ctl','ocn_UA','ocn_COARE']
    casenames_ocn = {'ocn_ctl':
                     '20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil',
                     'ocn_UA':
                     '20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil',
                     'ocn_COARE':
                     '20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil'}
    shortnames_atmo = ['atmo_ctl','atmo_UA','atmo_COARE']
    casenames_atmo = {'atmo_ctl':
                     '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl',
                     'atmo_UA':
                     '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl',
                     'atmo_COARE':
                     '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl'}
    #
    # Load OHC data.
    print('Reading OHC data...\n')
    OHC = {}
    for c in shortnames_ocn:
        OHC[c] = global_OHC_timeseries(casenames_ocn[c])
    #
    # Load salinity restoring data.
    print('Reading salinity restoring data...\n')
    salRes = {}
    for c in shortnames_ocn:
        salRes[c] = global_salRes_timeseries(casenames_ocn[c])
    #
    # Load velocity magnitude data.
    print('Reading velocity magnitude data...\n')
    velMag = {}
    for c in shortnames_ocn:
        velMag[c] = global_velMag_timeseries(casenames_ocn[c])
    #
    # Load other data.
    data_atmo = {}
    for c in shortnames_atmo:
        data_atmo[c] = xr.open_dataset('~/Data/E3SM/' + casenames_atmo[c] +
                                       '/atm/' + casenames_atmo[c] +
                                       '_EAM_customGlobalStats.nc')
    data_ocn = {}
    for c in shortnames_ocn:
        data_ocn[c] = xr.open_dataset('~/Data/E3SM/' + casenames_ocn[c] +
                                      '/ocn/' + casenames_ocn[c] +
                                      '_mpaso_customGlobalStats.nc')
    #
    # Sort out some bad data in ocn_ctl data.
    for c in ['ocn_ctl']:
        data_ocn[c]['timeMonthly_avg_evaporationFlux'].data = \
            np.where(data_ocn[c]['timeMonthly_avg_evaporationFlux'].data > 0.0,
                     np.nan,
                     data_ocn[c]['timeMonthly_avg_evaporationFlux'].data)
        data_ocn[c]['timeMonthly_avg_activeTracers_temperature'].data = \
            np.where(abs(data_ocn[c]\
                         ['timeMonthly_avg_activeTracers_temperature'].data) \
                     > 35.0,
                     np.nan,
                     data_ocn[c]\
                     ['timeMonthly_avg_activeTracers_temperature'].data)
        data_ocn[c]['timeMonthly_avg_activeTracers_salinity'].data = \
            np.where(abs(data_ocn[c]\
                         ['timeMonthly_avg_activeTracers_salinity'].data) \
                     > 50.0,
                     np.nan,
                     data_ocn[c]\
                     ['timeMonthly_avg_activeTracers_salinity'].data)
    #
    # Print out results.
    print('Sign convention: positive heat flux means ocean gains energy.')
    print('---------- atmo AND ocn ----------')
    print('----- LHFLX')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(data_ocn[c]['timeMonthly_avg_latentHeatFlux'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(-1.0*data_atmo[c]['LHFLX'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- SHFLX')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(data_ocn[c]['timeMonthly_avg_sensibleHeatFlux'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(-1.0*data_atmo[c]['SHFLX'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- QFLX')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(24.0*60.0*60.0*\
                               data_ocn[c]\
                               ['timeMonthly_avg_evaporationFlux'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(-1.0*24.0*60.0*60.0*\
                               data_atmo[c]['QFLX'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- FTNS')
    for c in shortnames_ocn:
        print(c)
        netHeatFlux = data_ocn[c]['timeMonthly_avg_sensibleHeatFlux'] + \
                      data_ocn[c]['timeMonthly_avg_latentHeatFlux'] + \
                      data_ocn[c]['timeMonthly_avg_shortWaveHeatFlux'] + \
                      data_ocn[c]['timeMonthly_avg_longWaveHeatFluxUp'] + \
                      data_ocn[c]['timeMonthly_avg_longWaveHeatFluxDown']
        data_ocn[c]['netHeatFlux'] = netHeatFlux
        print("{:+.3f}".format(netHeatFlux.\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(data_atmo[c]['FTNS'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('---------- atmo only ----------')
    print('----- PRECT_GLOBAL')
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(1000.0*24.0*60.0*60.0*\
                               data_atmo[c]['PRECT_GLOBAL'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- TREFHT')
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(-273.15 + \
                               data_atmo[c]['TREFHT'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- QREFHT')
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(1000.0*\
                               data_atmo[c]['QREFHT'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- U10')
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(data_atmo[c]['U10'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('----- TREFHT - TS')
    for c in shortnames_atmo:
        print(c)
        print("{:+.3f}".format(data_atmo[c]['TSDIFF'].\
                               sel(time=slice(st_time_atmo,end_time_atmo)).\
                               mean(dim='time').data))
    print('---------- ocn only ----------')
    print('----- SST')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(data_ocn[c]\
                               ['timeMonthly_avg_activeTracers_temperature'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    print('----- SSS')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(data_ocn[c]\
                               ['timeMonthly_avg_activeTracers_salinity'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    print('----- SSH  (last year average)')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(100.0*
                               data_ocn[c]\
                               ['timeMonthly_avg_pressureAdjustedSSH'].\
                               sel(Time=slice(st_time_ssh,end_time_ssh)).\
                               mean(dim='Time').data))
    print('----- OHC (last month relative to initial condition)')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(OHC[c].data[-1]))
    print('----- SSS restoring (PSU per year)')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3g}".format(365.0*24.0*60.0*60.0*\
                               salRes[c].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    print('----- zonal velocity')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(100.0*\
                               data_ocn[c]\
                               ['timeMonthly_avg_velocityZonal'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    print('----- meridional velocity')
    for c in shortnames_ocn:
        print(c)
        print("{:+.3f}".format(100.0*\
                               data_ocn[c]\
                               ['timeMonthly_avg_velocityMeridional'].\
                               sel(Time=slice(st_time_ocn,end_time_ocn)).\
                               mean(dim='Time').data))
    #
    # Print out LaTeX table.
    LaTeX_table(shortnames_atmo, shortnames_ocn,
                data_atmo, data_ocn, OHC, salRes, velMag,
                st_time_atmo, end_time_atmo,
                st_time_ocn, end_time_ocn,
                st_time_ssh, end_time_ssh)
    
    return

################################################################################
# Helper functions.

#------------------------------------------
# Gets global OHC time series for case "cn".
def global_OHC_timeseries(cn):
    #
    # Read data.
    filename = '~/Data/E3SM/' + cn + '/ocn/regionAveragedOHCAnomaly.nc'
    ds = xr.open_dataset(filename)
    # Get global (region number 7) and sum over depths.
    ohc = ds.ohc[:,6,:].sum(dim='nVertLevels')
    ohc.attrs['units'] = '$10^{22}$ J'
    #
    return ohc

#------------------------------------------
# Gets global salinity restoring time series for case "cn".
def global_salRes_timeseries(cn):
    #
    # Read data.
    filename = '~/Data/E3SM/' + cn + '/ocn/' + cn + \
               '_mpaso_customGlobalStats_meanAbsolute.nc'
    ds = xr.open_dataset(filename)
    # Get time series.
    salRes = ds['timeMonthly_avg_salinitySurfaceRestoringTendency']
    #
    return salRes

#------------------------------------------
# Gets global surface velocity magnitude time series for case "cn".
def global_velMag_timeseries(cn):
    #
    # Read data.
    filename = '~/Data/E3SM/' + cn + '/ocn/' + cn + \
               '_mpaso_customGlobalStats_meanAbsolute.nc'
    ds = xr.open_dataset(filename)
    # Get time series.
    vel_mag = ds[['timeMonthly_avg_velocityZonal',
                  'timeMonthly_avg_velocityMeridional']]
    #
    return vel_mag

#------------------------------------------
# Prints out results formatted in LaTeX table(s). 
def LaTeX_table(shortnames_atmo, shortnames_ocn,
                data_atmo, data_ocn, OHC, salRes, velMag,
                st_time_atmo, end_time_atmo,
                st_time_ocn, end_time_ocn,
                st_time_ssh, end_time_ssh):
    #
    # Three tables:
    #     1. Fluxes from atmo and ocn models.
    #     2. atmo model changes (precip etc.)
    #     3. ocn model changes (salinity etc.)
    #
    #-----------------------------------------
    # atmo and ocn
    # Print header.
    print('\\begin{table}[ht]')
    print('\\caption{INSERT CAPTION HERE}')
    print('\\centering')
    print('\\begin{tabular}{p{5cm} p{2cm} c c c}')
    #
    # Print data.
    print('  &  & control & UA & COARE  \\\\')
    print('\\hline')
    print('$Q_{E}\ \ (W~m^{-2})$  & atmo & ' +
          "{:+.2f}".format(-1.0*\
                           data_atmo['atmo_ctl']['LHFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(-1.0*\
                           data_atmo['atmo_UA']['LHFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(-1.0*\
                           data_atmo['atmo_COARE']['LHFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:+.2f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_latentHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_latentHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_latentHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$Evaporation\ \ (mm~day^{-1})$  & atmo & ' +
          "{:.2f}".format(24.0*60.0*60.0*\
                           data_atmo['atmo_ctl']['QFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(24.0*60.0*60.0*\
                           data_atmo['atmo_UA']['QFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(24.0*60.0*60.0*\
                           data_atmo['atmo_COARE']['QFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:.2f}".format(-1.0*24.0*60.0*60.0*\
                           data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_evaporationFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:.2f}".format(-1.0*24.0*60.0*60.0*\
                           data_ocn['ocn_UA']\
                           ['timeMonthly_avg_evaporationFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:.2f}".format(-1.0*24.0*60.0*60.0*\
                           data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_evaporationFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$Q_{H}\ \ (W~m^{-2})$  & atmo & ' +
          "{:+.2f}".format(-1.0*\
                           data_atmo['atmo_ctl']['SHFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(-1.0*\
                           data_atmo['atmo_UA']['SHFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(-1.0*\
                           data_atmo['atmo_COARE']['SHFLX'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:+.2f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_sensibleHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_sensibleHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_sensibleHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$R_{s,net}\ \ (W~m^{-2})$  & atmo & ' +
          "{:+.1f}".format(data_atmo['atmo_ctl']['FSNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.1f}".format(data_atmo['atmo_UA']['FSNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.1f}".format(data_atmo['atmo_COARE']['FSNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:+.1f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_shortWaveHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.1f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_shortWaveHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.1f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_shortWaveHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$R_{l,down}\ \ (W~m^{-2})$  & atmo & ' +
          "{:+.1f}".format(data_atmo['atmo_ctl']['FLDS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.1f}".format(data_atmo['atmo_UA']['FLDS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.1f}".format(data_atmo['atmo_COARE']['FLDS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:+.1f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_longWaveHeatFluxDown'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.1f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_longWaveHeatFluxDown'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.1f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_longWaveHeatFluxDown'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$R_{l,up}\ \ (W~m^{-2})$  & atmo & ' +
          "{:+.1f}".format(-1.0*\
                           data_atmo['atmo_ctl']['FLNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data \
                           + \
                           -1.0*\
                           data_atmo['atmo_UA']['FLDS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.1f}".format(-1.0*\
                           data_atmo['atmo_UA']['FLNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data \
                           + \
                           -1.0*\
                           data_atmo['atmo_UA']['FLDS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.1f}".format(-1.0*\
                           data_atmo['atmo_COARE']['FLNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data \
                           + \
                           -1.0*\
                           data_atmo['atmo_COARE']['FLDS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:+.1f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_longWaveHeatFluxUp'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.1f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_longWaveHeatFluxUp'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.1f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_longWaveHeatFluxUp'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$Q_{net}\ \ (W~m^{-2})$  & atmo & ' +
          "{:+.2f}".format(data_atmo['atmo_ctl']['FTNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(data_atmo['atmo_UA']['FTNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(data_atmo['atmo_COARE']['FTNS'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print(' & ocn & ' +
          "{:+.2f}".format(data_ocn['ocn_ctl']['netHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_UA']['netHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_COARE']['netHeatFlux'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    #
    # Print end of table. 
    print('\\end{tabular}')
    print('\\label{flux_table}')
    print('\\end{table}')
    #
    #-----------------------------------------
    # atmo only
    # Print header.
    print('\\begin{table}[ht]')
    print('\\caption{INSERT CAPTION HERE}')
    print('\\centering')
    print('\\begin{tabular}{p{6cm} c c c}')
    #
    # Print data.
    print('  & control & UA & COARE  \\\\')
    print('\\hline')
    print('$Precip\ \ (mm~day^{-1})$  & ' +
          "{:.2f}".format(1000.0*24.0*60.0*60.0*\
                           data_atmo['atmo_ctl']['PRECT_OCEAN'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(1000.0*24.0*60.0*60.0*\
                           data_atmo['atmo_UA']['PRECT_OCEAN'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(1000.0*24.0*60.0*60.0*\
                           data_atmo['atmo_COARE']['PRECT_OCEAN'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print('$Precip\ \ (global)^{a}$  & ' +
          "{:.2f}".format(1000.0*24.0*60.0*60.0*\
                           data_atmo['atmo_ctl']['PRECT_GLOBAL'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(1000.0*24.0*60.0*60.0*\
                           data_atmo['atmo_UA']['PRECT_GLOBAL'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(1000.0*24.0*60.0*60.0*\
                           data_atmo['atmo_COARE']['PRECT_GLOBAL'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print('$T_{2m}\ \ (^{\circ}C)$  & ' +
          "{:+.2f}".format(-273.15 + \
                           data_atmo['atmo_ctl']['TREFHT'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(-273.15 + \
                           data_atmo['atmo_UA']['TREFHT'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(-273.15 + \
                           data_atmo['atmo_COARE']['TREFHT'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print('$q_{2m}\ \ (g~kg^{-1})$  & ' +
          "{:.2f}".format(1000.0*\
                           data_atmo['atmo_ctl']['QREFHT'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(1000.0*\
                           data_atmo['atmo_UA']['QREFHT'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(1000.0*\
                           data_atmo['atmo_COARE']['QREFHT'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print('$U_{10m}\ \ (m~s^{-1})$  & ' +
          "{:.2f}".format(data_atmo['atmo_ctl']['U10'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(data_atmo['atmo_UA']['U10'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:.2f}".format(data_atmo['atmo_COARE']['U10'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print('$(T_{2m}-T_{s})\ \ (^{\circ}C)$  & ' +
          "{:+.2f}".format(data_atmo['atmo_ctl']['TSDIFF'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(data_atmo['atmo_UA']['TSDIFF'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' & ' +
          "{:+.2f}".format(data_atmo['atmo_COARE']['TSDIFF'].\
                           sel(time=slice(st_time_atmo,end_time_atmo)).\
                           mean(dim='time').data) +
          ' \\\\')
    print('\\hline')
    print('\multicolumn{4}{l}~$^{a}$ For land and ocean combined. \\\\')
    #
    # Print end of table. 
    print('\\end{tabular}')
    print('\\label{atmo_table}')
    print('\\end{table}')
    #-----------------------------------------
    # ocn only
    # Print header.
    print('\\begin{table}[ht]')
    print('\\caption{INSERT CAPTION HERE}')
    print('\\centering')
    print('\\begin{tabular}{p{6cm} c c c}')
    #
    # Print data.
    print('  & control & UA & COARE  \\\\')
    print('\\hline')
    print('$SST\ \ (^{\circ}C)$ & ' +
          "{:+.2f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_activeTracers_temperature'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_activeTracers_temperature'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_activeTracers_temperature'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$SSS\ \ (PSU)$ & ' +
          "{:+.2f}".format(data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_activeTracers_salinity'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_UA']\
                           ['timeMonthly_avg_activeTracers_salinity'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_activeTracers_salinity'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$\Delta SSH\ \ (cm)^{a}$ & ' +
          "{:+.2f}".format(100.0*\
                           data_ocn['ocn_ctl']\
                           ['timeMonthly_avg_pressureAdjustedSSH'].\
                           sel(Time=slice(st_time_ssh,end_time_ssh)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(100.0*\
                           data_ocn['ocn_UA']\
                           ['timeMonthly_avg_pressureAdjustedSSH'].\
                           sel(Time=slice(st_time_ssh,end_time_ssh)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(100.0*\
                           data_ocn['ocn_COARE']\
                           ['timeMonthly_avg_pressureAdjustedSSH'].\
                           sel(Time=slice(st_time_ssh,end_time_ssh)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$\Delta OHC\ \ (10^{22}~J)^{b}$ & ' +
          "{:+.3f}".format(OHC['ocn_ctl'].data[-1]) +
          ' & ' +
          "{:+.3f}".format(OHC['ocn_UA'].data[-1]) +
          ' & ' +
          "{:+.3f}".format(OHC['ocn_COARE'].data[-1]) +
          ' \\\\')
    print('$|SSS\ restoring|\ \ (m~PSU~yr^{-1})$  & ' +
          "{:+.3g}".format(365.0*24.0*60.0*60.0*\
                           salRes['ocn_ctl'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.3g}".format(365.0*24.0*60.0*60.0*\
                           salRes['ocn_UA'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.3g}".format(365.0*24.0*60.0*60.0*\
                           salRes['ocn_COARE'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$|u|\ \ (cm~s^{-1})$ & ' +
          "{:+.2f}".format(100.0*\
                           velMag['ocn_ctl']\
                           ['timeMonthly_avg_velocityZonal'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(100.0*\
                           velMag['ocn_UA']\
                           ['timeMonthly_avg_velocityZonal'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(100.0*\
                           velMag['ocn_COARE']\
                           ['timeMonthly_avg_velocityZonal'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('$|v|\ \ (cm~s^{-1})$ & ' +
          "{:+.2f}".format(100.0*\
                           velMag['ocn_ctl']\
                           ['timeMonthly_avg_velocityMeridional'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(100.0*\
                           velMag['ocn_UA']\
                           ['timeMonthly_avg_velocityMeridional'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' & ' +
          "{:+.2f}".format(100.0*\
                           velMag['ocn_COARE']\
                           ['timeMonthly_avg_velocityMeridional'].\
                           sel(Time=slice(st_time_ocn,end_time_ocn)).\
                           mean(dim='Time').data) +
          ' \\\\')
    print('\\hline')
    SSH_time_str = ('$^{a}$Average SSH over ' +
                    'model year 10'+
                    ' relative to initial condition.')
    print('\multicolumn{4}{l}~' + SSH_time_str + ' \\\\')
    OHC_time_str = ('$^{b}$OHC in ' +
                    'December of model year 10' +
                    ' relative to initial condition.')
    print('\multicolumn{4}{l}~' + OHC_time_str + ' \\\\')
    #
    # Print end of table. 
    print('\\end{tabular}')
    print('\\label{ocn_table}')
    print('\\end{table}')

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
