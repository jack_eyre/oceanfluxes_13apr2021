"""
Plot time series of ocean global mean freshwater fluxes from different 
model runs. 

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
import calendar
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Load data.
    print('\nReading data...\n')
    evap_data,evap_names = load_data(
        'timeMonthly_avg_sumGlobalStats_evaporationFluxSum')
    #
    # Convert to accumulated differences.
    plot_data, plot_names = accum_diff(evap_data, evap_names)
    for c in plot_data:
        print(c[-12:].mean())
    #
    # Plot results.
    plt.plot(plot_data[0].Time, plot_data[0],
             'r', label=plot_names[0])
    plt.plot(plot_data[1].Time, plot_data[1],
             'b--', label=plot_names[1])
    plt.plot(plot_data[2].Time, plot_data[2],
             'k:', label=plot_names[2])
    plt.legend(loc='upper right')
    plt.show()
    #
    return

def load_data(var_name):
    #
    # Define cases.
    cases = ['20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil',
             '20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil',
             '20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil']
    case_names = ['ctl',
                  'UA',
                  'COARE']
    #
    # Initialize variable to hold data.
    case_data = []
    # Load files.
    for c in cases:
        data_dir = '~/Data/E3SM/' + c + '/ocn/hist/'
        fn_tail = '_mpaso.hist.am.timeSeriesStatsMonthly_' + \
                  'GlobalEvaporationFluxSum.nc'
        ds = xr.open_dataset(data_dir + c + fn_tail)
        var_data = ds[var_name]
        # Trick to fill in bad data in ctl case.
        var_data.data = np.where(np.abs(ds[var_name]) < 1.0,
                                 np.nan,ds[var_name])
        var_data = var_data.interpolate_na(dim='Time',
                                           method='linear',
                                           use_coordinate=False)
        case_data.append(var_data)
    #
    return case_data,case_names

def load_gl_area():
    c = '20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil'
    ds = xr.open_dataset('~/Data/E3SM/' +
                         c +
                         '/ocn/hist/' +
                         c +
                         '_mpaso.hist.am.timeSeriesStatsMonthly_' + \
                         'GlobalEvaporationFluxSum.nc')
    gl_area = ds['timeMonthly_avg_areaCellGlobal'].data[0]
    return(gl_area)

def accum_diff(e_data, e_names):
    #
    gl_area = load_gl_area()
    #
    #### Accumulate ####
    #
    a_data = e_data.copy()
    #
    # Loop over cases then times.
    for ic,c in enumerate(e_data):
        cumsum = 0.0
        mm = 1
        for it in range(len(c.Time)):
            # Get days in month - uses noleap calendar!
            d_in_m = calendar.monthrange(2001, mm)[1]
            # Get total evaporation for this month.
            monthevap = c.data[it]*(d_in_m*24.0*60.0*60.0/gl_area)
            # Add this to previous total and put in accumulated array.
            a_data[ic].data[it] = cumsum + monthevap
            # Update cumulative sum and month.
            cumsum = cumsum + monthevap
            if mm < 12:
                mm = mm + 1
            else:
                mm = 1
    #### Differences ####
    #
    # Define permutations.
    plus =  [1,2,2]
    minus = [0,0,1]
    #
    # Define arrays to hold results.
    diff_data = []
    diff_names = []
    # Loop over permutations.
    for p in range(len(plus)):
        diff_data.append(a_data[plus[p]] - a_data[minus[p]])
        diff_names.append(e_names[plus[p]] + ' - ' + e_names[minus[p]])
    #
    return diff_data,diff_names
    
################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
