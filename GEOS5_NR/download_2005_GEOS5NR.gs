**** The program - calls function for each date period.

** Specify date period.
time1 = "18:20Z15JUNE2006"
time2 = "23:40Z15JUNE2006"
** Specify variable.
var = "taux"
** Specify file name.
fnm = "2006_06_15_18-00_taux.nc"
** Call function for instantaneous data.
rs = wrt_nc_tavg(time1,time2,var,fnm)
** Confirm that function has run for this period.
say rs
** Specify variable.
var = "tauy"
** Specify file name.
fnm = "2006_06_15_18-00_tauy.nc"
** Call function for instantaneous data.
rs = wrt_nc_tavg(time1,time2,var,fnm)
** Confirm that function has run for this period.
say rs


****----------------------------------------------------------------------------
**** The functions, used above to actually get and write the data.

function wrt_nc_tavg(t1,t2,varb,fname)

** Define opendap server for dataset.
'sdfopen https://opendap.nccs.nasa.gov/dods/OSSE/G5NR/Ganymed/7km/0.0625_deg/tavg/tavg30mn_2d_met2_Nx'

** Set lat and long limits.
'set lat -90 90'
'set lon 0 360'
** Set time limits, based on arguments passed from main program.
'set time 't1' 't2 
** Specify which variable to write.
'define 'varb' = 'varb

** Set file name, and to write in floating point (instead of double precision).
'set sdfwrite -flt /home/jeyre/Data/model/'fname

** Set variable attributes.
'set sdfattr 'varb' String units N m-2'
if (varb = "taux")
    'set sdfattr 'varb' String long_name eastward_surface_wind_stress'
endif
if (varb = "tauy")
    'set sdfattr 'varb' String long_name northward_surface_wind_stress'
endif

** Set global attributes.
'set sdfattr global String title 2d,30-Minute,Time-Averaged,Single-Level,Full Resolution,Single-Level Surface Diagnostics'
'set sdfattr global String Conventions COARDS, GrADS'
'set sdfattr global String dataType Grid'
'set sdfattr global String history Thu Aug 16 09:03:09 EDT 2018 : imported by GrADS Data Server 2.0'

** Write out the file.
'sdfwrite 'varb

** Define return string to confirm file name.
ret_str = "File written: "fname
** End of function -- return string. 
return(ret_str)

****----------------------------------------------------------------------------